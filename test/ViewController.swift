//
//  ViewController.swift
//  test
//
//  Created by Sergey Yefanov on 01.03.15.
//  Copyright (c) 2015 Sergey Yefanov. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet var tableView : UITableView?
    @IBOutlet var scrollView : UIScrollView?
    @IBOutlet var pageControl : UIPageControl?
    var dataSource = []

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        scrollView?.pagingEnabled = true
        scrollView?.bounces = false
        
        dataSource = [[UIImage(named: "1.jpg")!, UIImage(named: "2.jpg")!],
                      [UIImage(named: "3.jpeg")!, UIImage(named: "4.jpeg")!, UIImage(named: "5.jpg")!]]
        
        loadScrollView(0)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataSource.count
    }


    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell?
        
        if (cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        }
        
        cell?.textLabel?.text = "up \(indexPath.row)"
        
        return cell!;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        loadScrollView(indexPath.row)
    }
    
    func loadScrollView(index : Int)
    {
        for view in scrollView!.subviews
        {
            view.removeFromSuperview()
        }
        
        let arr = dataSource[index] as [UIImage]
        pageControl?.numberOfPages = arr.count
        scrollView?.contentSize = CGSize(width: self.view.frame.width * CGFloat(arr.count), height: scrollView!.frame.height)
        
        scrollView?.contentOffset = CGPointZero
        
        for var i = 0; i < arr.count; i++
        {
            let imageView = UIImageView(image: arr[i])
            
            let point = CGPoint(x: self.view.frame.width * CGFloat(i), y: 0)
            let size = CGSize(width: scrollView!.frame.size.width, height: scrollView!.frame.size.height)
            imageView.frame = CGRect(origin: point, size: size)
            scrollView?.addSubview(imageView)
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let contentOffeset = scrollView.contentOffset.x
        let width = self.view.frame.width
        let page = Int(contentOffeset / width)
        pageControl?.currentPage = page
    }
    
}

